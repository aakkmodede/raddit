class Link < ActiveRecord::Base
  acts_as_votable
  belongs_to :user, dependent: :destroy
  has_many   :comments, dependent: :destroy
end
